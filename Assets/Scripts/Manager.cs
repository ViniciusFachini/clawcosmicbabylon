using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Manager : MonoBehaviour
{
    #region Permission
    public bool podePegar; // Essa bool se for falsa, o usuario não pega de jeito nenhum, e se essa bool for true e o usuario tiver sorte e precisão, "talvez", por algumas ocasioens ele pode ate pegar mas é bem dificil do mesmo jeito kkkk
    #endregion

    #region Components Fields
    public GameObject menuCanvas;
    public GameObject popUp;
    public AudioListener audioListener;
    public Text scorePoints_txt;
    public Sprite[] sondSprite;
    public Image buttomSound;
    public GameObject winObj;
    public GameObject looseObj;
    #endregion

    #region Private Variables
    [SerializeField] private bool gameIsOn_ = false;
    [SerializeField] private bool soundIsOn_ = true;
    [SerializeField] private bool gameWon_ = false;
    [SerializeField] private bool alreadyWon_ = false;
    [SerializeField] private bool alreadyLoose_ = false;
    [SerializeField] private bool gameLoose_ = false;
    #endregion

    #region Events
    public Action On_GameWon;
    public Action On_GameLoose;
    #endregion

    #region Singleton
    public static Manager instance { get { return instance_; } }
    private static Manager instance_;
    #endregion

    #region MonoBehaviour Functions
    private void Awake()
    {
        instance_ = this;
        On_GameWon += OnGameWon;
        On_GameLoose += OnGameLoose;
    }

    void Update()
    {
        if (gameWon_)
        {
            if (!alreadyLoose_ && !alreadyWon_)
            {
                gameWon_ = false;
                alreadyWon_ = true;
                
                StartCoroutine(GameWonCoroutine());
            }
        }

        else if (gameLoose_)
        {
            if (!alreadyWon_ && !alreadyLoose_)
            {
                gameLoose_ = false;
                alreadyLoose_ = true;
                
                StartCoroutine(GameLooseCoroutine());
            }
        }
    }
    #endregion

    #region Menu Functions [CALL BY BUTTONS]
    //Marca o in�cio do jogo e desativa o Canvas do Menu
    public void Click_PlayButton() => menuCanvas.SetActive(!(gameIsOn_ = true));

    //Ativa e desativa o audioListener do Jogo, soundIsOn_ indica o estado atual
    public void Click_SoundControlButton()
    {
        audioListener.enabled = (soundIsOn_ = !soundIsOn_);
        if(soundIsOn_){buttomSound.sprite = sondSprite[0];}else{buttomSound.sprite = sondSprite[1];}
    }

    //Sai do Jogo
    public void Click_ExitButton() => Application.Quit();
    #endregion

    #region Private Functions
    IEnumerator GameWonCoroutine()
    {
        yield return new WaitForSeconds(0.1f);
        SoundControll.instance.PlayVictory();
        winObj.gameObject.SetActive(true);
        popUp.SetActive(true);
        alreadyWon_ = true;
        On_GameWon?.Invoke();
        yield break;
    }

    IEnumerator GameLooseCoroutine()
    {
        yield return new WaitForSeconds(0.1f);
        looseObj.gameObject.SetActive(true);
        SoundControll.instance.PlayDefeat();
        popUp.SetActive(true);
        On_GameLoose?.Invoke();
        yield break;
    }

    #endregion

    #region Callbacks Server
    private void OnGameWon()
    {
      Debug.Log("Ganhei!");
    }

    private void OnGameLoose()
    {
        Debug.Log("ROLETA ;-;");
    }


    #endregion

    #region Public Functions [Script Conversation]
    public bool Get_GameIsOn() => gameIsOn_;
    public bool Get_GameWon() => gameWon_;
    public void Set_GameWon(bool won) => gameWon_ = true;
    public bool Get_GameLoose() => gameWon_;
    public void Set_GameLoose(bool loose) => gameLoose_ = true;
    public bool Get_AlreadyWon() => alreadyWon_;
    public bool Get_AlreadyLoose() => alreadyLoose_;
    public void ResetAlready()
    {
        alreadyWon_ = false;
        alreadyLoose_ = false;
    }
    #endregion
}
