using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;

public class FixedObject : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    private Vector3 screenPoint;
    private Vector3 offset;
    private Vector3 scanPos;
    [SerializeField] private Image arco;
    [SerializeField] private Image pointer;
    [SerializeField] private Transform clawObject;
    [SerializeField] private Transform twitter;
    [SerializeField] private GameObject viewAngle;
    [SerializeField] private Transform clawing;
    [SerializeField] private Transform claw;
    [SerializeField] private float clawY;
    [SerializeField] private Transform impulse;
    [SerializeField] private Transform[] garra;
    [SerializeField] private float levelGarra;
    [SerializeField] private float levelMax;
    [SerializeField] public Transform access;
    [SerializeField] public bool finalize;
    [SerializeField] public Transform positionX;
    [SerializeField] public Transform positionZ;
    [SerializeField] public Transform suportMoveX;
    [SerializeField] public Transform containerMoveZ;
    [SerializeField] public Gerator gerator;
    [SerializeField] private BoxCollider box;
     [SerializeField] private GameObject modal;
    [SerializeField] private GameObject success;
    [SerializeField] private GameObject fail;

    private Vector3 vectorX;
    private Vector3 vectorZ;
    private Vector3 speed;
    private bool canWin;
    [HideInInspector]public bool waiting;
    private bool canLooseFirst;
    private Vector3 retorno;
    public float smootf;
    public bool clawGet;
    public bool clawSet;
    public Animator anim;
    public Animator getAnim;
    private Color on = new Color(1, 1, 1, 1);
    private Color off = new Color(1, 1, 1, 0);
    private Color offLow = new Color(1, 1, 1, 0.3f);
    private bool noControll;
    public bool up, down, left, right, stop;
    public Image imaUp, imaDown, imaLeft, imaRight;
    public bool stack;
    public GET get;
    public BoxCollider winBox;
    public CameraPosition cameraPosition;
    public GameObject block;
    public GameObject Menu;
    public GameObject Cilind;

    void Start()
    {
        arco.gameObject.SetActive(false);
        pointer.gameObject.SetActive(false);
        anim.Play("stop");
    }

    void Update()
    {
        CheckIfLoose();
        scanPos = pointer.transform.position;
        impulse.transform.position = new Vector3(claw.transform.position.x, clawY, claw.transform.position.z);
        CameraLerp();
        Euler();
        if (finalize)
        {
            FinalizePlay();
        }
        if (clawGet && !clawSet && !stack)
        {
            claw.transform.position = Vector3.MoveTowards(claw.transform.position, impulse.transform.position, Time.deltaTime);
            levelMax = -20;
            if (claw.transform.position.y == impulse.transform.position.y)
            {
                StartCoroutine("GetEnding");
                clawGet = false;
                clawSet = true;
                stack = true;

            }
        }
        if (clawGet && clawSet && stack)
        {
            claw.transform.position = Vector3.MoveTowards(claw.transform.position, retorno, Time.deltaTime);
            if (claw.transform.position.y == retorno.y)
            {

                clawSet = false;
                clawGet = false;
                winBox.enabled = true;
                finalize = true;
            }
        }
        
    }
    public void OnPointerDown(PointerEventData pointerEventData)
    {
        if (!noControll)
        {
            arco.transform.position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
            pointer.transform.position = arco.transform.position;
            screenPoint = Camera.main.WorldToScreenPoint(scanPos * -1);
            offset = arco.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
            arco.gameObject.SetActive(true);
            pointer.gameObject.SetActive(true);
            imaDown.raycastTarget = true;
            imaUp.raycastTarget = true;
            imaLeft.raycastTarget = true;
            imaRight.raycastTarget = true;
            viewAngle.SetActive(false);
        }
    }
    public void OnDrag(PointerEventData data)
    {
        if (!noControll)
        {
            Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
            Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
            pointer.transform.position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        }
    }
    public void OnPointerUp(PointerEventData pointerEventData)
    {
        pointer.transform.position = arco.transform.position;
        arco.gameObject.SetActive(false);
        pointer.gameObject.SetActive(false);
        right = false;
        left = false;
        up = false;
        down = false;
        stop = true;
        imaDown.raycastTarget = false;
        imaUp.raycastTarget = false;
        imaLeft.raycastTarget = false;
        imaRight.raycastTarget = false;
        viewAngle.SetActive(true);
        anim.Play("stop");
    }
    public void EnterMaxX()
    {

        right = true;
        left = false;
        up = false;
        down = false;
        stop = false;
        anim.Play("right");
    }
    public void EnterMinX()
    {

        right = false;
        left = true;
        up = false;
        down = false;
        stop = false;
        anim.Play("left");
    }
    public void EnterMinY()
    {

        right = false;
        left = false;
        up = false;
        down = true;
        stop = false;
        anim.Play("down");
    }
    public void EnterMaxY()
    {

        right = false;
        left = false;
        up = true;
        down = false;
        stop = false;
        anim.Play("up");
    }
    void CameraLerp()
    {
        if(Menu.activeSelf == false)
        {
            Vector3 positionDrop = Vector3.zero;
            positionDrop.x = Mathf.SmoothDamp(twitter.transform.position.x, clawObject.transform.position.x, ref speed.x, smootf);
            positionDrop.y = Mathf.SmoothDamp(twitter.transform.position.y, clawObject.transform.position.y, ref speed.y, smootf);
            positionDrop.z = Mathf.SmoothDamp(twitter.transform.position.z, clawObject.transform.position.z, ref speed.z, smootf);
            Vector3 newPosition = new Vector3(positionDrop.x, positionDrop.y, positionDrop.z);
            twitter.transform.position = Vector3.Lerp(twitter.transform.position, newPosition, Time.time);
        }
        
    }
    public void GET()
    {
        SoundControll.instance.PlayButtonPress();
        getAnim.Play("down");
       
        if (access.childCount == 0 && finalize == false)
        {
            if (!clawGet && !clawSet)
            {
                noControll = true;
                StartCoroutine("Ative");
                vectorX = suportMoveX.transform.position;
                vectorZ = containerMoveZ.transform.position;
                box.enabled = true;
                clawGet = true;
                clawSet = false;
                retorno = claw.transform.position;
            }
        }
        StartCoroutine(playTwice());
       
    }

    IEnumerator GetEnding()
    {
        levelMax = 10;
        yield return new WaitForSeconds(1f);
        clawSet = true;
        clawGet = true;
    }
    public void startEuler()
    {
        StartCoroutine("GetEnding");
    }
    void Euler()
    {
        garra[0].localEulerAngles = new Vector3(garra[0].localEulerAngles.x, garra[0].localEulerAngles.y, levelGarra);
        garra[1].localEulerAngles = new Vector3(garra[1].localEulerAngles.x, garra[1].localEulerAngles.y, levelGarra);
        garra[2].localEulerAngles = new Vector3(garra[2].localEulerAngles.x, garra[2].localEulerAngles.y, levelGarra);
        if (levelGarra > levelMax)
        {
            levelGarra -= Time.deltaTime * 40;
        }
        if (levelGarra < levelMax)
        {
            levelGarra += Time.deltaTime * 40;
        }
    }
    public void ResetConfig()
    {
        clawGet = false;
        clawSet = false;
        finalize = false;
        levelMax = 0;
        canWin = false;
        canLooseFirst = false;
        waiting = false;
        noControll = false;
        stack = false;
        success.SetActive(false);
        fail.SetActive(false);
        modal.SetActive(false);
        get.box.enabled = true;
        winBox.enabled = false;
        StartCoroutine("destroi");
        cameraPosition.angle = 12;
        cameraPosition.left = false;
        cameraPosition.right = false;
        cameraPosition.tr = cameraPosition.parent.GetChild(12);
        block.SetActive(true);
        Manager.instance.ResetAlready();
    }
    void FinalizePlay()
    {
        if (cameraPosition.angle > 12)
        {
            cameraPosition.left = true;
        }
        else
        {
            cameraPosition.left = false;
        }
        if(cameraPosition.angle < 12)
        {
            cameraPosition.right = true;
        }
        else
        {
            cameraPosition.right = false;
        }
        
        
        float maxZ = positionZ.transform.position.z + 0.1f;
            float minZ = positionZ.transform.position.z - 0.1f;
            vectorZ = Vector3.MoveTowards(vectorZ, positionZ.transform.position, Time.deltaTime);
            containerMoveZ.transform.position = new Vector3(containerMoveZ.transform.position.x, containerMoveZ.transform.position.y, vectorZ.z);

        if (containerMoveZ.transform.position.z < maxZ && containerMoveZ.transform.position.z > minZ)
        {
            vectorX = Vector3.MoveTowards(vectorX, positionX.transform.position, Time.deltaTime);
            suportMoveX.transform.position = new Vector3(vectorX.x, suportMoveX.transform.position.y, suportMoveX.transform.position.z);
            float maxX = positionX.transform.position.x + 0.1f;
            float minX = positionX.transform.position.x - 0.1f;
            if (suportMoveX.transform.position.x < maxX && suportMoveX.transform.position.x > minX)
            {
                SoundControll.instance.PlayGarraTryCatch();
                levelMax = -10;
                if (access.childCount != 0)
                {
                    access.GetChild(0).transform.SetParent(gerator.gameObject.transform);
                }
                //noControll = false;
                finalize = false;
            }
        }
    }

    private void CheckIfLoose()
    {
        if (!Manager.instance.Get_AlreadyWon())
        {
            if (finalize) canWin = true;

            if (canWin && !finalize && access.childCount <= 0 && !waiting && !canLooseFirst)
            {
                Manager.instance.Set_GameLoose(true);
                canWin = false;
               // Debug.Log("Eu 1");
            }

            if (access.childCount > 0)
            {
                canLooseFirst = true;
            }
          
            else if (canWin && !finalize && canLooseFirst && !waiting)
            {
                waiting = true;
                StartCoroutine(WaitTime());
            }
        }
    }

    IEnumerator playTwice()
    {
        SoundControll.instance.PlayGarraTryCatch();
        yield return new WaitForSeconds(1.7f);
        SoundControll.instance.PlayGarraTryCatch();
        yield break;
    }
    IEnumerator WaitTime()
    {
        yield return new WaitForSeconds(1.5f);

        if (Manager.instance.Get_AlreadyWon())
        {
           // Debug.Log("DUDE");
            canWin = false;
            canLooseFirst = false;
            waiting = false;
            yield break;
        }

        else
        {
            Manager.instance.Set_GameLoose(true);
            canWin = false;
            waiting = false;
            canLooseFirst = false;
            yield break;
        }
    }
    IEnumerator destroi()
    {
        for(int i = gerator.amount - 1; i > -1; i--)
        {
            Destroy(gerator.gameObject.transform.GetChild(i).gameObject);
        }
        gerator.Awake();
        yield return new WaitForSeconds(0); 

    }
    IEnumerator Ative()
    {
        yield return new WaitForSeconds(0.3f); 
        Cilind.SetActive(true);
        StartCoroutine("Desative");
        
    }
     IEnumerator Desative()
     {
        yield return new WaitForSeconds(2.5f); 
        Cilind.SetActive(false);

     }

}
