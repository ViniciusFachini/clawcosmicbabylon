using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClawAnimatorController : MonoBehaviour
{
    public Animator clawAnimator;
    [SerializeField] private bool isOpening;
    [SerializeField] private bool isClosing;

    void Update()
    {
        if (ClawMovement.instance.Get_Current_Movement_Status() && (!isOpening && !isClosing))
        {
            clawAnimator.ResetTrigger("close");
            clawAnimator.ResetTrigger("open");
            clawAnimator.ResetTrigger("idle");
            clawAnimator.SetTrigger("move");
        }

        else if (!ClawMovement.instance.Get_Current_Movement_Status() && (!isOpening && !isClosing))
        {
            clawAnimator.ResetTrigger("close");
            clawAnimator.ResetTrigger("open");
            clawAnimator.ResetTrigger("move");
            clawAnimator.SetTrigger("idle");
        }

        else if (!ClawMovement.instance.Get_Current_Movement_Status() && (isOpening && !isClosing))
        {
            clawAnimator.ResetTrigger("close");
            clawAnimator.ResetTrigger("move");
            clawAnimator.ResetTrigger("idle");
            clawAnimator.SetTrigger("open");
        }

        else if (!ClawMovement.instance.Get_Current_Movement_Status() && (!isOpening && isClosing))
        {
            clawAnimator.ResetTrigger("move");
            clawAnimator.ResetTrigger("idle");
            clawAnimator.ResetTrigger("open");
            clawAnimator.SetTrigger("close");
        }
    }
}
