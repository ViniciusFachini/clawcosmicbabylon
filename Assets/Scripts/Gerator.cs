using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gerator : MonoBehaviour
{   
    [SerializeField]public int amount;
    [SerializeField]private float timer;
    [SerializeField]private GameObject[] positions;
    [SerializeField]private GameObject[] patineteID;
    [SerializeField]private Transform content;
    [SerializeField]private FixedObject fixedObject;
    [SerializeField]private Manager manager;




    public void Awake()
    {
        StartCoroutine("Patinetes");
    }
    IEnumerator Patinetes() 
    {
        int r = Random.Range(0, 5);
        for(int i = 0; i < amount; i++)
        {
            GameObject patinete = Instantiate(patineteID[r], positions[i].transform.position, Quaternion.identity) as GameObject;
            patinete.transform.SetParent(transform);
            patinete.transform.localEulerAngles = positions[i].transform.localEulerAngles + new Vector3(Random.Range(-20, 20), Random.Range(-45, 45), Random.Range(-45, 45));
            patinete.GetComponent<Rigidbody>().AddForce(Random.Range(-20, 20), Random.Range(0, 20), Random.Range(-20, 20));
            patinete.GetComponent<StartEuler>().fixedObject = fixedObject;
            patinete.GetComponent<StartEuler>().gerator = GetComponent<Gerator>();
            patinete.GetComponent<StartEuler>().manager = manager;
            r += 1; if(r > 4){r = 0;}
            yield return new WaitForSeconds(timer); 
        }
    }
}
