using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClawMovement : MonoBehaviour
{

    #region Public Variables
    [Header("Claw Base Movement")]
    [Range(0.00f,5)]
    public float speedClawBase;
    public float max_RangeClawBase = -0.1f;
    public float min_RangeClawBase = -1.6f;
   

    [Header("Claw Movement")]
    [Space(45)]
    [Range(0.00f, 5)]
    public float speedClaw;
    public float max_RangeClaw = 0.93f;
    public float min_RangeClaw = -0.93f;

    [Header("Components")]
    [Space(45)]
    public GameObject baseClaw;
    public GameObject claw;
    public RectTransform origin;
    public RectTransform joystick;
    public FixedObject fixedObject;
   
    #endregion


    #region Private Variables
    private Vector3 previousV3BaseClaw;
    private Vector3 previousV3Claw;
    private bool isMoving;
    private bool up,back,left,right;
    private Vector3 lastPosition;
    private bool isDisplacing;
    private bool playOnce;
    #endregion


    #region Lambda Expressions
    private bool GoingFoward() => Input.GetKey(KeyCode.W);
    private bool GoingBack() => Input.GetKey(KeyCode.S);
    private bool GoingLeft() => Input.GetKey(KeyCode.A);
    private bool GoingRight() => Input.GetKey(KeyCode.D);

    #endregion


    #region Singleton
    public static ClawMovement instance { get { return instance_; } }
    private static ClawMovement instance_;
    private void Awake() => instance_ = this;
    #endregion

    void Update()
    {
        if (!Manager.instance.Get_AlreadyWon() || !Manager.instance.Get_AlreadyLoose())
        {
            if (Manager.instance.Get_GameIsOn() && !fixedObject.finalize)
            {
                if (fixedObject.clawGet == false && fixedObject.clawSet == false)
                {
                    BaseClawControl();
                    ClawControl();
                }

            }
        }

        if (claw.transform.position != lastPosition)
        {
            isDisplacing = true;

            if (!playOnce)
            {
                playOnce = true;
                SoundControll.instance.PlayGarraStart();
            }
            
        }
        else
        {
            isDisplacing = false;
            playOnce = false;
            SoundControll.instance.StopGarraStart();
        }

        lastPosition = claw.transform.position;

        if (isDisplacing)
            SoundControll.instance.PlayGarraLoop();
        else
            SoundControll.instance.StopGarraLoop();


    }


    #region Base Claw Movement
    private void BaseClawControl()
    { 
        //Base ir para frente 
        if ((GoingFoward() || fixedObject.up) && baseClaw.transform.localPosition.z < max_RangeClawBase)  baseClaw.transform.localPosition = new Vector3(baseClaw.transform.localPosition.x, baseClaw.transform.localPosition.y, baseClaw.transform.localPosition.z + (Time.deltaTime * speedClawBase));

        //Base ir para tras
        if ((GoingBack() || fixedObject.down) && baseClaw.transform.localPosition.z > min_RangeClawBase) baseClaw.transform.localPosition = new Vector3(baseClaw.transform.localPosition.x, baseClaw.transform.localPosition.y, baseClaw.transform.localPosition.z + (Time.deltaTime * -speedClawBase)); 
    }
    #endregion

    #region Claw Movement
    private void ClawControl()
    {
        //Base ir para direita
        if ((GoingRight() || fixedObject.right) && claw.transform.localPosition.x < max_RangeClaw) claw.transform.localPosition = new Vector3(claw.transform.localPosition.x  + (Time.deltaTime * speedClaw), claw.transform.localPosition.y, claw.transform.localPosition.z);

        //Base ir para esquerda
        if ((GoingLeft() || fixedObject.left) && claw.transform.localPosition.x > min_RangeClaw)  claw.transform.localPosition = new Vector3(claw.transform.localPosition.x + (Time.deltaTime * -speedClaw), claw.transform.localPosition.y, claw.transform.localPosition.z);
    }
    #endregion

    #region Movement Status
    public bool Get_Current_Movement_Status() => isMoving = (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D) || fixedObject.right || fixedObject.left || fixedObject.up || fixedObject.down);
    #endregion

}
