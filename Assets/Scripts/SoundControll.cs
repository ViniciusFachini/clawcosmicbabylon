using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundControll : MonoBehaviour
{
    public AudioSource garraStart;
    public AudioSource garraLoop;
    public AudioSource buttonPress;
    public AudioSource garraTryCatch;
    public AudioSource victory;
    public AudioSource defeat;

    public static SoundControll instance { get { return instance_; } }
    private static SoundControll instance_;
    private void Awake() => instance_ = this;

    private void PlaySoundClip(AudioSource a)
    {
        a.mute = false;
        a.PlayOneShot(a.clip);
    }

    private void StopSoundClip(AudioSource a) => a.mute = true;
    private void PlaySoundTrack(AudioSource a) => a.mute = false;
    private void StopSoundTrack(AudioSource a) => a.mute = true;

    public void PlayGarraStart() => PlaySoundClip(garraStart);
    public void StopGarraStart() => StopSoundTrack(garraStart);

    public void PlayGarraLoop() => PlaySoundTrack(garraLoop);
    public void StopGarraLoop() => StopSoundTrack(garraLoop);

    public void PlayButtonPress() => PlaySoundClip(buttonPress);
    public void StopButtonPress() => StopSoundClip(buttonPress);

    public void PlayGarraTryCatch() => PlaySoundClip(garraTryCatch);
    public void StopGarraTryCatch() => StopSoundClip(garraTryCatch);

    public void PlayVictory() => PlaySoundClip(victory);

    public void PlayDefeat() => PlaySoundClip(defeat);



}
