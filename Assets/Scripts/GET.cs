using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GET : MonoBehaviour
{
    public FixedObject fixedObject;
    public BoxCollider box;
    private bool stack;
  
    void OnTriggerEnter(Collider col)
    {
        if(col.CompareTag("Player"))
        {
            
            fixedObject.clawGet = false;
            fixedObject.startEuler();
            box.enabled = false;
            fixedObject.stack = true;     
        }
    }
}
