using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public state camState;
    public Transform target;
    public float lerpCamera = 5;
    public float pivotY;

    public float z_Offset = 3;
    public float limit_Z = -7.8f;

    private float z_Final;

    public enum state
    {
        Follow,
        LookAt,
    }

    void Update()
    {
        if (camState == state.Follow)
        {
            z_Final = target.transform.position.z - z_Offset;
            if (z_Final <= limit_Z) z_Final = limit_Z;

            transform.position = Vector3.Lerp(transform.position, new Vector3(target.transform.position.x, target.transform.position.y, z_Final), lerpCamera * Time.deltaTime);
        }

        else
        {
            transform.LookAt(target.transform.position);
        }
    }
}
