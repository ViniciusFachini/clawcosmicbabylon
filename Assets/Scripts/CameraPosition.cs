using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPosition : MonoBehaviour
{
    public int angle = 12;
    public float timer;    
    public Animator animator;
    public Transform tr;
    public Transform parent;
    public GameObject Cam;
    public bool left, right;

    private Vector3 speed;
    public float smootf;


    void Start()
    {
        
    }

    void Update()
    {
        State();
        CentralizeCam();
    }
    public void OnDownRight()
    {
        animator.Play("goRight");
        right = true;
    }
    public void OnUpRight()
    {
        animator.Play("backRight");
        right = false;
        timer = 0;
    }
    public void OnDownLeft()
    {
        animator.Play("goLeft");
        left = true;

    }
    public void OnUpLeft()
    {
        animator.Play("backLeft");
        timer = 0;
        left = false;
    }
    void State()
    {
        if(left)
        {
            if(angle > 0)
            {
                timer += Time.deltaTime * 5;
                if(timer > 1)
                {
                
                    angle -= 1;
                    tr = parent.GetChild(angle);
                    timer = 0;
                }
            }
        }
        if(right)
        {
            if(angle < 24)
            {
                timer += Time.deltaTime * 5;
                if(timer > 1)
                {
               
                    angle += 1;
                    tr = parent.GetChild(angle);
                    timer = 0;
                
                }
            }
        }
    }
    void CentralizeCam()
    {
        Vector3 positionDrop = Vector3.zero;
		positionDrop.x = Mathf.SmoothDamp(Cam.transform.position.x, tr.transform.position.x, ref speed.x, smootf);
		positionDrop.y = Mathf.SmoothDamp(Cam.transform.position.y, tr.transform.position.y, ref speed.y, smootf);
        positionDrop.z = Mathf.SmoothDamp(Cam.transform.position.z, tr.transform.position.z, ref speed.z, smootf);
		Vector3 newPosition = new Vector3(positionDrop.x, positionDrop.y, positionDrop.z);
		Cam.transform.position = Vector3.Lerp(Cam.transform.position, newPosition, Time.time);
    }
}
