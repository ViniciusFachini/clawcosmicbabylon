using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartEuler : MonoBehaviour
{
    public Rigidbody rb;
    private int i = 3;
    public Gerator gerator;
    public FixedObject fixedObject;
    public Manager manager;
    private bool ok;
    public BoxCollider box;
    public CapsuleCollider capsole;
    private float timer = 2.3f;
    void Awake()
    {
        StartCoroutine("Desable");
    }
    IEnumerator Desable()
    {
        yield return new WaitForSeconds(i); 
        //rb.isKinematic = true;
        rb.mass = 3;
        i = 2;
    }
    void OnTriggerEnter(Collider col)
    {
        if(col.CompareTag("GameController"))
        {
            if(!ok && fixedObject.access.childCount == 0)
            {
                StartCoroutine("Geting");
            }
            if(!manager.podePegar)
            {
                Vector3 versao = fixedObject.positionZ.transform.position;
                Vector3 atual = transform.position;
                if(atual.x < 0)
                {
                    timer = Random.Range(1.5f, 2f);
                }
                else
                {
                    timer = Random.Range(1.8f, 3f);
                }

                StartCoroutine("ExitClaw");
            }
        }

        if (col.CompareTag("WinTrigger"))
        {
            Manager.instance.Set_GameWon(true);
        }
        if(col.CompareTag("Finish"))
        {
            gameObject.SetActive(false);
        }
    }
    void OnTriggerExit(Collider col)
    {
        if(col.CompareTag("GameController"))
        {
            
            transform.SetParent(gerator.transform);
            StartCoroutine("Desable");
            
        }
    }
    IEnumerator ExitClaw()
    {
        yield return new WaitForSeconds(timer);
        if(transform.parent == fixedObject.access)
        {
            box.enabled = false;
            capsole.enabled = false;
            transform.SetParent(gerator.transform);
        }
        yield return new WaitForSeconds(0.2f);
        box.enabled = true;
        capsole.enabled = true;
      
    }
    IEnumerator Geting()
    {
        yield return new WaitForSeconds(0.2f);
        transform.SetParent(fixedObject.access);
        rb.isKinematic = false;
        ok = true;
    }


}
