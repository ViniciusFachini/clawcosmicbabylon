using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Activation : MonoBehaviour
{
    void OnEnable()
    {
        StartCoroutine("Activate");
    }

    IEnumerator Activate()
    {
        yield return new WaitForSeconds(2.5f);
        gameObject.SetActive(false);
    }

 
}
