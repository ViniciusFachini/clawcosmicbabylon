Shader "MyCustomShader/ColorChannelSpecular"
{Properties {
      _Color ("Main Color", Color) = (1,1,1,1)
      _HueColor ("Hue Shift Color", Color) = (1,1,1,1)
	  [Space(20)]
      _MainTex ("Main Texture", 2D) = "white" {}
	  [Space(35)]
	  _NormalTex ("Normal Texture", 2D) = "bump" {}
	  [Space(35)]
	  _Glossiness ("Smoothness", Range(0,1)) = 0.5
	  _Specular ("Specular", Range(0,1)) = 0.5
	   [Space(35)]
	  FT_SliderMatriz ("Hue Shift Slider",Range(0,26)) = 0

	  [Space(35)]
	  _RimColor ("Rim Color", Color) = (0,0,0,0)
      _RimPower ("Rim Power", Range(0.5,8.0)) = 3.0

    }
   SubShader{

   Lighting Off 
      CGPROGRAM
	  
	      #pragma surface surf StandardSpecular fullforwardshadows

	      struct Input {
	          float2 uv_MainTex : TEXCOORD0;
			  float3 worldPos;
			  float3 viewDir;
	      };

	      sampler2D _MainTex;
		  sampler2D _NormalTex;
		  fixed4 _Color;
		  fixed4 _HueColor;
		  half _NormalMapMultiplier1;
		  float4 _RimColor;
	      float _RimPower;
		  int FT_SliderMatriz;
		  half _Glossiness;
		  half _Specular;


	fixed4 ReturnMatrixColor(fixed4 color, int slider){
		 
		 switch(slider) {

		  case 1: color = color.brra; break;
		  case 2: color = color.brga; break;
		  case 3: color = color.brba; break;
		  case 4: color = color.bgra; break;
		  case 5: color = color.bgga; break;
		  case 6: color = color.bgba; break;
		  case 7: color = color.bbra; break;
		  case 8: color = color.bbga; break;
		  case 9: color = color.bbba; break;
		  
		  case 10: color = color.grra; break;
		  case 11: color = color.grga; break;
		  case 12: color = color.grba; break;
		  case 13: color = color.ggra; break;
		  case 14: color = color.ggga; break;
		  case 15: color = color.ggba; break;
		  case 16: color = color.gbra; break;
		  case 17: color = color.gbga; break;
		  case 18: color = color.gbba; break;

		  case 19: color = color.rrra; break;
		  case 20: color = color.rrga; break;
		  case 21: color = color.rrba; break;
		  case 22: color = color.rgra; break;
		  case 23: color = color.rgga; break;
		  case 24: color = color.rbra; break;
		  case 25: color = color.rbga; break;
		  case 26: color = color.rbba; break;

		  }
		  
		  return color;
	  }
	
	 void surf (Input IN, inout SurfaceOutputStandardSpecular  o) {
		 float3 localPos = IN.worldPos -  mul(unity_ObjectToWorld, float4(0,0,0,1)).xyz;

			fixed4 tex1 = tex2D(_MainTex , IN.uv_MainTex) * _HueColor;
			fixed4 col = ReturnMatrixColor(tex1,FT_SliderMatriz) * _Color;


			o.Albedo = col;
			o.Specular = _Specular;
			o.Smoothness = _Glossiness;

			o.Normal = UnpackScaleNormal (tex2D (_NormalTex, IN.uv_MainTex), 1).rgb;
		    half rim = 1.0 - saturate(dot (normalize(IN.viewDir), o.Normal));
            o.Emission = _RimColor.rgb * pow (rim, _RimPower);
			
 
	  }


      ENDCG

    } 
    //Fallback "Diffuse"
}

